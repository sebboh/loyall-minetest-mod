This minetest mod adds two blocks.  When they are placed adjacently,
they resemble a stereo or record player.  One block contains code that
makes it play music when the player hits it to activate it.  The
player can deactivate the music by hitting it again.  It plays a
single music file with a hard-coded filename (patches welcome) and I
didn't include that file because I don't have a license to
redistribute it.

NOTE: I don't actually speak Lua!