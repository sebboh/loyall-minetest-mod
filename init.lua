minetest.register_node("loyall:stereo_left", {
    description = "Left side of Stero",
    tiles = {
        --0 //top
        "stereo_left_top.png",
        --1 //bottom
        "stereo_bottom.png",
        --2 //back
        "stereo_right_back.png",
        --3
        "stereo_left_front.png",
        --4
        "stereo_bottom.png",
        --5
        "stereo_bottom.png"
    },
    groups = {cracky = 1}
})

minetest.register_node("loyall:stereo_right", {
    description = "Right side of Stero",
    tiles = {
        "stereo_right_top.png",
        "stereo_bottom.png",
        "stereo_left_back.png",
        "stereo_right_front.png",
        "stereo_bottom.png",
        "stereo_bottom.png"
    },
    groups = {cracky = 1}
})

--from https://forum.minetest.net/viewtopic.php?f=9&t=1412
soundblocks_music={} --This is needed because you cannot save the handle of the sound in param2 of the node

minetest.register_node("loyall:musicblock", {
    description = "Music Block",
    tile_images = {"default_wood.png"},
    groups = {snappy=2,choppy=2,oddly_breakable_by_hand=2},
})


--Some functions that you need to save the handle in soundblocks_music:
function soundblocks_get_handle(pos)
    local i=0
    while soundblocks_music[i]~=nil do
        if soundblocks_music[i].pos.x==pos.x and soundblocks_music[i].pos.y==pos.y and soundblocks_music[i].pos.z==pos.z then
            return soundblocks_music[i].handle
        end
        i=i+1
    end
    return 0
end

function soundblocks_set_handle(pos, handle)
    local i=0
    while soundblocks_music[i]~=nil do
        if soundblocks_music[i].pos.x==pos.x and soundblocks_music[i].pos.y==pos.y and soundblocks_music[i].pos.z==pos.z then
            soundblocks_music[i].handle=handle
            return
        end
        i=i+1
    end
    soundblocks_music[i]={}
    soundblocks_music[i].pos=pos
    soundblocks_music[i].handle=handle
end


--This is the actual code that plays the sound:
minetest.register_on_punchnode(function(pos, node, puncher)
    if node.name=="loyall:stereo_right" then
        if soundblocks_get_handle(pos)==0 then
            local handle=0
            handle = minetest.sound_play("03_Istanbul_Not_Constantinople", { --name of sound, file name extension is .ogg
            pos = pos, --pos where sound comes from
            gain = 10.0,
            max_hear_distance = 2560,}) --sound gets lower the farer you get away from the jukebox
            soundblocks_set_handle(pos, handle)
        else
            minetest.sound_stop(soundblocks_get_handle(pos))
            soundblocks_set_handle(pos, 0)
        end
    end
end)
